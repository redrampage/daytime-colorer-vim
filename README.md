# Daytime colorer

Vim plugin that changes color theme according to outside natural illumination.

Illumination calculated using current system time and pre-set geo-coordinates(no internet access required). 

# Installation

Just put the daytimecolorer.vim into .vim/plugin/
or
If you're using pathogen just clone this repo to you bundle dir
```
git clone https://bitbucket.org/redrampage/daytime-colorer-vim.git ~/.vim/bundle/daytime-colorer-vim.git
```
# Configuration

Define following variables in your vimrc:
```
" Scheme definitions
let g:dtcDayScheme = "pyte"
let g:dtcNightScheme = "wombat256"

" Following schemes are optional.
let g:dtcDawnScheme = "zenburn" "if none - day scheme will be used
let g:dtcDuskScheme = "zenburn" "if none - night scheme will be used

" Geographical coordinates to calculate sun position
" Easiest way to obtain those is:
"  * Open google maps
"  * Navigate to your desired place
"  * Click the "Link" button and copy the 'll' variable from location's url
let g:dtcLatitude = 59.935  "Positive for North negative for South
let g:dtcLongitude = 30.325 "Positive for East and negative for West

" Local timezone
let g:dtcTimeOffset = 4 " GMT+4
" let g:dtcTimeOffset = -4.5 " GMT-04:30
```
# About

This plugin uses the algorithm from "Almanac for Computers, 1990", description has been found at:

http://williams.best.vwh.net/sunrise_sunset_algorithm.htm

Automatic refresh code taken from:

http://www.vim.org/scripts/script.php?script_id=2658